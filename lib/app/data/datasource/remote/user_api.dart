import 'dart:convert';

import 'package:ejemplo_tres/app/domain/models/responses/users_response.dart';
import 'package:ejemplo_tres/app/domain/models/user_model.dart';
import 'package:http/http.dart' as http;

class UserAPI {
  Future<Map<String, dynamic>> getUsers() async {
    try {
      var response = await http
          .get(
        Uri.parse("https://reqres.in/api/users?page=1"),
      )
          .timeout(const Duration(seconds: 30), onTimeout: () {
        return Future.error("No se puede conectar al servidor");
      });
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body) as Map<String, dynamic>;
        UsersResponse userResponse = UsersResponse.fromJson(jsonData);
        Map<String, dynamic> mapa = {'users': userResponse.data, 'error': null};
        return mapa;
      } else {
        Map<String, dynamic> mapa = {
          'users': null,
          'error': "ERROR" + response.statusCode.toString()
        };
        return mapa;
      }
    } catch (error) {
      Map<String, dynamic> mapa = {
        'users': null,
        'error': "ERROR" + error.toString()
      };
      return mapa;
    }
  }

  Future<Map<String, dynamic>> getUserByPage(int page) async {
    try {
      var response = await http.get(
        Uri.parse("https://reqres.in/api/users?page=$page"),
      );
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body) as Map<String, dynamic>;
        UsersResponse userResponse = UsersResponse.fromJson(jsonData);
        Map<String, dynamic> mapa = {'users': userResponse.data, 'error': null};
        return mapa;
      } else {
        Map<String, dynamic> mapa = {
          'users': null,
          'error': "ERROR: " + response.statusCode.toString()
        };
        return mapa;
      }
    } catch (error) {
      Map<String, dynamic> mapa = {
        'users': null,
        'error': error.toString(),
      };
      return mapa;
    }
  }

  Future<Map<String, dynamic>> createUser(
      String name, String job, String lastName) async {
    try {
      var body = {'name': name, 'job': job};
      var response = await http.post(
        Uri.parse("https://reqres.in/api/users"),
        body: json.encode(body),
      );
      if (response.statusCode == 201) {
        var jsonData = jsonDecode(response.body) as Map<String, dynamic>;
        User user = User(
          avatar: "https://reqres.in/img/faces/3-image.jpg",
          email: "$name@$lastName.com",
          firstName: name,
          id: int.parse(jsonData['id']),
          lastName: lastName,
        );
        Map<String, dynamic> mapa = {'user': user, 'error': null};
        return mapa;
      } else {
        Map<String, dynamic> mapa = {
          'users': null,
          'error': "ERROR: " + response.statusCode.toString()
        };
        return mapa;
      }
    } catch (error) {
      Map<String, dynamic> mapa = {
        'users': null,
        'error': error.toString(),
      };
      return mapa;
    }
  }
}
