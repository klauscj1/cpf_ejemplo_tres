import 'package:ejemplo_tres/app/data/datasource/remote/user_api.dart';
import 'package:ejemplo_tres/app/domain/repositories/user_repository.dart';

class UserRepositoryImpl extends UserRepository {
  final UserAPI _api = UserAPI();

  @override
  Future<Map<String, dynamic>> getUsers() => _api.getUsers();

  @override
  Future<Map<String, dynamic>> getUsersByPage(int page) =>
      _api.getUserByPage(page);

  @override
  Future<Map<String, dynamic>> createUser(
          {required String name,
          required String job,
          required String lastName}) =>
      _api.createUser(name, job, lastName);
}
