abstract class UserRepository {
  Future<Map<String, dynamic>> getUsers();
  Future<Map<String, dynamic>> getUsersByPage(int page);
  Future<Map<String, dynamic>> createUser(
      {required String name, required String job, required String lastName});
}
