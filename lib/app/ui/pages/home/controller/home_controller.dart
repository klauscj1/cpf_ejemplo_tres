import 'package:ejemplo_tres/app/data/repositories_impl/user_repostory_impl.dart';
import 'package:ejemplo_tres/app/domain/models/user_model.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

class HomeController extends SimpleNotifier {
  HomeController() {
    _init();
  }
  //Variables
  List<User> _users = [];
  bool _loading = true;
  String _error = "";

  //Get's
  List<User> get users => _users;
  bool get loading => _loading;
  String get error => _error;

//Set's
  set users(List<User> list) {
    _users = list;
    notify();
  }

  set loading(bool value) {
    _loading = value;
    notify();
  }

  set error(String value) {
    _error = value;
    notify();
  }

//methods own
  Future<void> _init() async {
    Map<String, dynamic> mapa = await UserRepositoryImpl().getUsers();
    if (mapa['error'] != null) {
      _error = mapa['error'];
    } else {
      _users = mapa['users'];
    }
    _loading = false;
    notify();
  }

  void addUser(User user) {
    List<User> oldUsers = _users;
    oldUsers.add(user);
    _users = oldUsers;
    notify();
  }
}
