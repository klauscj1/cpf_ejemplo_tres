import 'package:ejemplo_tres/app/ui/pages/home/controller/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

import 'widgets/custom_dialog.dart';

final homeProvider = SimpleProvider(
  (ref) => HomeController(),
);

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("API REST"),
        actions: [
          IconButton(
            onPressed: () {
              showDialog(
                barrierDismissible: false,
                context: context,
                builder: (_) => const CustomDialog(),
              );
            },
            icon: const Icon(Icons.add),
          )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Consumer(
            builder: (_, ref, __) {
              final homeController = ref.watch(homeProvider);
              return homeController.loading
                  ? const SizedBox(
                      width: double.infinity,
                      child: Text(
                        "Cargando...",
                        textAlign: TextAlign.center,
                      ),
                    )
                  : homeController.error.isEmpty
                      ? Expanded(
                          child: ListView.builder(
                            itemCount: homeController.users.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title: Text(
                                    homeController.users[index].firstName!),
                                subtitle:
                                    Text(homeController.users[index].lastName!),
                                leading: Image.network(
                                    homeController.users[index].avatar!),
                              );
                            },
                          ),
                        )
                      : Center(
                          child: Text(
                            homeController.error,
                          ),
                        );
            },
          ),
        ],
      ),
    );
  }
}
