import 'package:ejemplo_tres/app/data/repositories_impl/user_repostory_impl.dart';
import 'package:ejemplo_tres/app/ui/pages/home/home_page.dart';
import 'package:flutter/material.dart';

class CustomDialog extends StatefulWidget {
  const CustomDialog({
    Key? key,
  }) : super(key: key);

  @override
  State<CustomDialog> createState() => _CustomDialogState();
}

class _CustomDialogState extends State<CustomDialog> {
  final TextEditingController _controllerUserName = TextEditingController();
  final TextEditingController _controllerUserLastName = TextEditingController();
  final TextEditingController _controllerUserJob = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    _controllerUserName.dispose();
    _controllerUserLastName.dispose();
    _controllerUserJob.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("New user"),
      content: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text("User name"),
            ),
            TextField(
              controller: _controllerUserName,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text("User lastname"),
            ),
            TextField(
              controller: _controllerUserLastName,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text("User job"),
            ),
            TextField(
              controller: _controllerUserJob,
            ),
          ],
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text("Cancelar"),
        ),
        ElevatedButton(
          onPressed: () async {
            String _userName = _controllerUserName.text;
            String _userLastname = _controllerUserLastName.text;
            String _userJob = _controllerUserJob.text;

            Map<String, dynamic> respuesta = await UserRepositoryImpl()
                .createUser(
                    name: _userName, job: _userJob, lastName: _userLastname);
            if (respuesta['user'] != null) {
              homeProvider.read.addUser(respuesta['user']);
              Navigator.pop(context);
            } else {}
          },
          child: const Text("Guardar"),
        ),
      ],
    );
  }
}
