import 'package:flutter_meedu/meedu.dart';

class SplashController extends SimpleNotifier {
  SplashController() {
    _init();
  }
  //variables
  String version = '';

//methods
  void _init() async {
    await Future.delayed(const Duration(milliseconds: 1300));
    version = '1.0.0';
    notify();
  }
}
