import 'package:ejemplo_tres/app/ui/pages/splash/controller/splash_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

final splashProvider = SimpleProvider(
  (ref) => SplashController(),
);

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProviderListener<SplashController>(
      provider: splashProvider,
      onChange: (context, controller) {
        Navigator.pushReplacementNamed(context, 'home');
      },
      builder: (_, controller) {
        return Scaffold(
          body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text('Ejemplo 3'),
                Consumer(
                  builder: (_, ref, __) {
                    final splashController = ref.watch(splashProvider);
                    return Text(splashController.version);
                  },
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
