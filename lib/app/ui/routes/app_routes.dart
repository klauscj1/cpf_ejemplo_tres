import 'package:ejemplo_tres/app/ui/pages/home/home_page.dart';
import 'package:ejemplo_tres/app/ui/pages/splash/splash_page.dart';
import 'package:ejemplo_tres/app/ui/routes/routes.dart';
import 'package:flutter/cupertino.dart';

Map<String, Widget Function(BuildContext)> routes = {
  Routes.home: (_) => const HomePage(),
  Routes.splash: (_) => const SplashPage(),
};
